# NAME OF APPLICATION

BOXINATOR

# NAME OF CREATORS

Emil Clemedson,
Christian Andersz,
Patric Bjurman


# HOW TO RUN THIS APPLICATION

***Clone repo.***

***Go to directory.***

***Press path.***

*Type 'cmd'.*

**Press Enter.**


###### -- Terminal opens --

*Type 'code .' in terminal.*

**Press Enter.**


###### -- VS Code starts --

*Type 'npm install' in terminal.*

*Type 'npm run devStart' in terminal.*

**Press Enter.**

***See it start in browser.***

###### -- Enjoy! --
