import { useEffect, useState } from 'react'
import Selecter from '../../Components/Selecter/Selecter';
import { ShipmentEditType } from '../../Models/ShipmentsTypes'
import './NewOrderModal.scss'
import { useSelector } from 'react-redux';
import { RootState } from '../../store/reducers';
import { Country } from '../../Models/CountryTypes';
import { calculatePrice } from '../../utils/shipments/priceCalculator';
import store from '../../store';
import { fetchCountries } from '../../store/actions/countriesActions';

type Props = {
    isAuthenticated: boolean;
    onSubmit?: (s : ShipmentEditType) => any;
}

export default function NewOrderModal({ isAuthenticated, onSubmit } : Props) {
    // State for countries.
    const countriesData = useSelector((state : RootState) => state.countriesData);
    const [selectedWeight, setSelectedWeight] = useState(0)
    const [selectedCountry, setSelectedCountry] = useState()

    /**
     * Fetches countries.
     */
    useEffect(() => {
        store.dispatch(fetchCountries());
    }, [])


    /**
     * Adds new shipment with values from input.
     * @param e Event
     * @returns adds new shipment 
     */
    function handleOnSubmit(e : any) : void {
        e.preventDefault();
        if (!onSubmit) {return;}

        const boxColour : string = e.target.boxColor.value;
        const receiverName : string = e.target.receiverName.value;
        const email : string | undefined = (!isAuthenticated) ? e.target.email.value : undefined;
        const weightOption : number = parseInt(e.target.weightOptions.value)
        const destinationCountryId : number = parseInt(e.target.destinationCountry.value)

        onSubmit({boxColour, destinationCountryId, receiverName, weightOption, email });
    }

    /**
     * Checks if chosen country has same id as any country in db and sets chosen country to selectedcountry state.
     * @param countryId id of country that is chosen.
     */
    function handleCountryChange(countryId : string) {
        const country = countriesData.countries.find((c : Country) => c.id === parseInt(countryId))
        setSelectedCountry(country)
    }

    return (
        <div className="new-order-modal-div">
            <form onSubmit={handleOnSubmit}>
                {(!isAuthenticated) ? 
                    <div  className="new-order-modal-input-div">
                        <label>Email</label>
                        <input required type="email" name="email" placeholder="Email" />
                    </div> 
                : null }

                <div className="new-order-modal-input-div">
                    <label>Receiver Name:</label>
                    <input required name="receiverName"  type="text" placeholder="Receiver Name"/>
                </div>

                <div className="new-order-modal-input-div">
                    <label>Weight Options:</label>
                    <Selecter required onChange={(weight) => setSelectedWeight(parseInt(weight))} name="weightOptions" options={[{value: 1, text: "Basic 1kg"}, {value:2, text: "Humble 2kg"}, {value:5, text: "Deluxe 5kg"}, {value:8, text: "Premium 8kg"}]}/>
                </div>

                <div className="new-order-modal-input-div-inline">
                    <label>Box Color:</label>
                    <input required name="boxColor" type="color"></input>
                </div>

                <div className="new-order-modal-input-div">
                    <label>DestinationCountry:</label>
                    <Selecter required onChange={handleCountryChange} name="destinationCountry" options={countriesData.countries.map((country : Country) => ({ value:country.id, text:country.name}))}/>
                </div>

                <hr/>
                
                <p>Price: {calculatePrice(selectedWeight, selectedCountry)} kr</p>

                <button className="btn secondary" type="submit">Send Order</button>
            </form>  
        </div>
    )
}
