import { useEffect } from 'react'
import { useSelector } from 'react-redux';
import Loader from '../../Components/Loader/Loader';
import { ShipmentStatusType } from '../../Models/ShipmentsTypes';
import store from '../../store';
import { fetchDetailedShipments } from '../../store/actions/shipmentDetailsActions';
import { RootState } from '../../store/reducers';
import dateFormat from 'dateformat';
import './RecentOrderModal.scss'

type Props = {
    id: number,
    admin: string,
    onOrderCancel?: (id : number) => void
}

export default function RecentOrderModal({ id, admin, onOrderCancel } : Props) {

    // State for chosen shipment.
    const shipment = useSelector((state : RootState) => state.shipmentDetailsData);

    // fetches shipment details with id.
    useEffect(() => {
        store.dispatch(fetchDetailedShipments(id));
    }, [id])

    function handleOrderCancel() {
        if (onOrderCancel) {
            onOrderCancel(id);
        }
    }

    /**
     * If shipments are still loading. show loader until all shipments are loaded.
     */
    if (shipment.loading) {
        return <Loader/>
    }

    /**
     * If there is any error with loading shipments show error.
     */
    if (shipment.error) {
        return <p>{ shipment.error }</p>
    }

    return (
        <div className="order-model-div">
            <div>
                <p><b>Receiver Name: </b> { shipment.shipment?.receiverName }</p>
                <p><b>Destination Country: </b> { shipment.shipment?.destinationCountry?.name }</p>
                <p><b>Weight: </b> { shipment.shipment?.weightOption } kg</p>
                <p><b>Total Cost: </b> { shipment.shipment?.totalCost } kr</p>
                { admin !== 'Admin' ? null : <p><b>Customer Email:</b> { shipment.shipment?.user?.email }</p> }
            </div>
            <div>
                <h3>Status:</h3>
                { shipment.shipment?.shipmentStatus.map((status : ShipmentStatusType, i : number) => {
                    return (
                        <div key={i} className="order-model-status">
                            <p>{status.status}</p>
                            <p> {dateFormat(status.createdAt, "mmmm dS yyyy HH:MM") }</p>
                        </div>
                    )
                })}
                <hr />             
            </div>
            {(admin !== 'Admin' ) ? <button onClick={handleOrderCancel} className="btn secondary">Canceling Order</button> : null}
        </div>

    )
}