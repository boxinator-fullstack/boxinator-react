import { Country } from './CountryTypes'
import { Account } from './AccountTypes'

export interface ShipmentType {
    id: number;
    boxColour: string | null;
    destinationCountryId: number;
    receiverName: string | null;
    totalCost: number;
    userId: number;
    weightOption: number;
}

export interface ShipmentStatusType {
    status: string;
    createdAt: Date;
}

export interface ShipmentDetailedType {
    id: number;
    boxColour: string | null;
    destinationCountry: Country;
    receiverName: string | null;
    totalCost: number;
    userId: number;
    weightOption: number;
    user: Account;
    ShipmentStatus: ShipmentStatusType[],
}


export interface ShipmentEditType {
    email?: string;
    boxColour: string;
    destinationCountryId: number;
    receiverName: string;
    weightOption: number;
}