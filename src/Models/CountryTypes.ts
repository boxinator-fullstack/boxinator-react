export interface Country {
    id: number;
    name: string;
    countryMultiplier: number;
}

export interface CountryEdit {
    name: string;
    countryMultiplier: number;
}

export interface CountryCreate {
    name: string;
    countryMultiplier: number;
}