import { Country } from "./CountryTypes";

export interface Account {
    id: number,
    firstName: string,
    lastName: string,
    email: string,
    birthDate: Date,
    zipCode: number,
    contactnumber: string,
    countryId: number,
    country: Country;
}

export interface AccountEdit {
    id: number,
    firstName: string,
    lastName: string,
    email:string,
    birthDate: Date,
    zipCode: number,
    contactnumber: string,
    countryId: number,
}