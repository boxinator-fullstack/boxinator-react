import axios from "axios";
import { Account, AccountEdit } from "../Models/AccountTypes";
import { Country, CountryEdit, CountryCreate } from "../Models/CountryTypes";
import {
  ShipmentDetailedType,
  ShipmentEditType,
  ShipmentType,
} from "../Models/ShipmentsTypes";

//const BASE_URL = "https://localhost:44373/api/v1";
const BASE_URL = "https://boxinator-api.azurewebsites.net/api/v1";

/**
 * Sets user bearer token.
 * Checks if user is of type admin.
 */
export const api = {
  token: "",
  Admin: "",

  getConfig: function () {
    if (!this.token) {
        return {headers: {
          "Content-Type": "application/json",
          "Access-Control-Allow-Origin": "*"
        },
      }
    }
    return {
      headers: {
        Authorization: `Bearer ${this.token}`,
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*"
      },
    };
  },

  setToken: async function (getAccessTokenSilently: any) {
    if (this.token === "") {
      try {
        this.token = await getAccessTokenSilently();
      } catch {
        this.token = "";
      }
    }
  },

  /**
   * Gets all country data from DB.
   * @returns all country data.
   */
  getCountry: async function (): Promise<Country[]> {
    const res = await axios.get(`${BASE_URL}/settings/countries`);
    return res.data;
  },

  /**
   * Posts new country to DB.
   * @param newCountry values for new country.
   * @returns adds new country to db
   */
  postCountry: async function (newCountry: CountryCreate): Promise<Country> {
    const res = await axios.post(
      `${BASE_URL}/settings/countries`,
      newCountry,
      this.getConfig()
    );
    return res.data;
  },

  /**
   * Updates chosen country with new values in DB.
   * @param updatedCountry new values for country.
   * @param id Id for the chosen country.
   * @returns updates values for chosen country.
   */
  putCountry: async function (
    updatedCountry: CountryEdit,
    id: number
  ): Promise<undefined> {
    await axios.put(
      `${BASE_URL}/settings/countries/${id}`,
      updatedCountry,
      this.getConfig()
    );
    return;
  },

  /**
   * Get current user data.
   * @returns current user.
   */
  getAccount: async function (): Promise<Account> {
    const res = await axios.get(`${BASE_URL}/account`, this.getConfig());
    return res.data;
  },

  /**
   * Updates user in DB with new values.
   * @param updatedAccount new values for user.
   * @param id id for current user.
   * @returns updates values for user.
   */
  putAccount: async function (
    updatedAccount: AccountEdit,
    id: number
  ): Promise<undefined> {
    await axios.put(
      `${BASE_URL}/account/${id}`,
      updatedAccount,
      this.getConfig()
    );
    return;
  },

  /**
   * Post a new shipment to DB.
   * @param newShipment values for new shipment. 
   * @returns adds new shipment to DB.
   */
  postShipment: async function (
    newShipment: ShipmentEditType
  ): Promise<ShipmentType> {
    const res = await axios.post(
      `${BASE_URL}/shipments`,
      newShipment,
      this.getConfig()
    );
    return res.data;
  },

  /**
   * Get specific shipment by id.
   * @param id id for specific shipment.
   * @returns get a specific shipment.
   */
  getShipmentById: async function (id: number): Promise<ShipmentDetailedType> {
    const res = await axios.get(
      `${BASE_URL}/shipments/${id}`,
      this.getConfig()
    );
    return res.data;
  },

  /**
   * Gets all shipments from DB.
   * @returns All shipments.
   */
  getShipments: async function (): Promise<ShipmentType[]> {
    const res = await axios.get(`${BASE_URL}/shipments`, this.getConfig());
    return res.data;
  },

  /**
   * Gets all shipments with status COMPLETED.
   * @returns all completed shipments.
   */
  getCompletedShipments: async function (): Promise<ShipmentType[]> {
    const res = await axios.get(
      `${BASE_URL}/shipments/complete`,
      this.getConfig()
    );
    return res.data;
  },

  /**
   * Gets all shipments with status CANCELLED.
   * @returns all cancelled shipments.
   */
  getCancelledShipments: async function (): Promise<ShipmentType[]> {
    const res = await axios.get(
      `${BASE_URL}/shipments/cancelled`,
      this.getConfig()
    );
    return res.data;
  },

  /**
   * Posts shipments status to DB.
   */
  postShipmentStatuses: async function (id: number, newStatus: string) {
    await axios.post(
      `${BASE_URL}/shipments/shipmentstatus/${id}`,
      newStatus,
      this.getConfig()
    );
    return;
  },
};
