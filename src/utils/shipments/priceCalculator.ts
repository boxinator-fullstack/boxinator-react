import { Country } from "../../Models/CountryTypes";

// Standard fee for shipments.
const STANDARD_FEE = 200;

/**
 * Calculates price for shipment based on weight and country multiplier.
 * @param weights Weight of box
 * @param country Destination country.
 * @returns Price of shipment
 */
export function calculatePrice(weights : number, country: Country | undefined) : number{
    if (!country || !weights){
        return 0;
    }
    return STANDARD_FEE + weights * country.countryMultiplier;
} 