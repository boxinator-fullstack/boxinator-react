import { useState } from 'react'
import { Link, useHistory } from "react-router-dom";
import { useAuth0 } from '@auth0/auth0-react';
import './Header.scss';
import { api } from '../../utils/api';


export default function Header() {
    const [open, setOpen] = useState(false)
    const { user, logout, getIdTokenClaims, isAuthenticated } = useAuth0()
    const history = useHistory()

    function handleLogout() {
        setOpen(false)
        logout({returnTo: `${window.location.origin}/start`});
    }

    function handleGoToProfile() {
        setOpen(false)
        history.push('/profile')
    }

    function handleGoToAdmin() {
        setOpen(false)
        history.push('/admin')
    }

    return (
        <header>
            <Link className="header-title" to="/"> <h1>Boxinator</h1></Link>
            {(user) ?  
                <div className="header-profile">
                    <button onClick={() => setOpen(!open)} className={"header-profile-button"}>
                        <p>{user?.email}</p> 
                        <img src={user?.picture} alt=""/>
                    </button> 
                    <div className={(!open) ? "header-drop-down" : "header-drop-down header-drop-down-open"}>
                        <button onClick={handleGoToProfile}>Profile</button>
                        {api.Admin !== 'Admin' ? null : <button onClick={handleGoToAdmin}>Admin</button>}
                        <button onClick={handleLogout}>Logout</button>
                    </div>
                    {(open) ? <button className="header-drop-down-back" onClick={() => setOpen(false)} /> : null}
                </div> 
                :
                null
            }
        </header>
    )
}
