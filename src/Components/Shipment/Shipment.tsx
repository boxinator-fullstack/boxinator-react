import Loader from '../Loader/Loader';
import './Shipment.scss';

type Props = {
    boxColour: string | null;
    receiverName: string | null;
    totalCost: number;
    id: number;
    loading?: boolean; 
    onClick?: (id: number) => void;
}

export default function Shipment({id, boxColour, receiverName, totalCost, onClick, loading } : Props) {

    function handleClick() {
        if (onClick) {
            onClick(id);
        }
    }
    

    return (
        <button onClick={handleClick} className="shipment-item">
            {(loading) ?
            <Loader/> :
            <>
                <div className="shipment-color-box" style={(boxColour !== null) ? { background:boxColour } :  { background:"#000" }}/>
                <p className="shipment-name">{receiverName}</p>
                <p>{totalCost} kr</p>
            </>}
        </button>
    )
}
