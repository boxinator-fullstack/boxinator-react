import Loader from '../Loader/Loader';
import './Modal.scss'

type Props = {
    title? : string;
    children : any;
    open: boolean;
    loading?: boolean;
    error?: string;
    onCancel?: () => void;
}

export default function Modal({ title, loading, error, children, open, onCancel} : Props) {
    if (!open && !loading && !error) return null

    function onBackClick() {
        if (onCancel) {
            onCancel();
        }
    }

    return (
        <div className="modal" >
            <div className="modal-front">
                {(title) ? <h1>{title}</h1> : null}
                {(loading) ? <Loader/> :
                    (error) ? 
                        <p>{error}</p>
                        :
                        children
                }
            </div>
            <button className="modal-back" onClick={onBackClick}></button>
        </div>
    )
}
