import Loader from '../Loader/Loader';
import './Selecter.scss'


type Option = {
    value: number;
    text: string;
}

type Props = {
    options: Option[]; 
    name?: string; 
    onChange?: (value : string) => void;
    required?: boolean 
    loading?: boolean;
    defaultValue?: any;
}


export default function Selecter({ loading, defaultValue, required, options, name, onChange } : Props) {

    function handleOnChange(e : any) {
        if (!onChange) { return; }
        onChange(e.target.value)
    }

    return (
        <div className="select">
            {(loading) ?
                <Loader/>
            :
                <select defaultValue={(defaultValue) ? defaultValue : ""} required={required} name={name} onChange={handleOnChange}>
                    <option value="" disabled>Select your option</option>
                    { options.map(option => <option key={option.value} value={option.value}>{ option.text }</option>) }
                </select>}
        </div>
    )
}
