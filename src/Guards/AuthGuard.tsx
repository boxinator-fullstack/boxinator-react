
import { Redirect } from "react-router-dom";
import { api } from "../utils/api";

export enum UserType {
    LoggedOut,
    User,
    Admin
}

// checks if user is login, if not login redirect to setup page else render Component 
function AuthGuard(userTypeRequired : UserType,  Component : any){    
    return function(){
        if (userTypeRequired === UserType.LoggedOut && api.token !== "") {
            return <Redirect to="/"/>
        }
        if ((userTypeRequired === UserType.User || userTypeRequired === UserType.Admin) && api.token === "") {
            return <Redirect to="/start"/>
        }
        if (userTypeRequired === UserType.Admin && api.Admin !== "Admin") {
            return <Redirect to="/"/>
        }        
        return <Component/>
    } 
}
export default AuthGuard
