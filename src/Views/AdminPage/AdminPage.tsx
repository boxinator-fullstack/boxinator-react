import React, { useState, useEffect } from "react";
import "./Admin.scss";
import store from "../../store";
import {
  fetchCancelledShipments,
  fetchCompletedShipments,
  fetchShipments,
} from "../../store/actions/shipmentsActions";
import { useSelector } from "react-redux";
import { RootState } from "../../store/reducers";
import Loader from "../../Components/Loader/Loader";
import Shipment from "../../Components/Shipment/Shipment";
import { ShipmentType } from "../../Models/ShipmentsTypes";
import Modal from "../../Components/Modal/Modal";
import RecentOrderModal from "../../Modals/RecentModal/RecentOrderModal";
import {
  fetchCountries,
  putCountry,
} from "../../store/actions/countriesActions";
import { Country } from "../../Models/CountryTypes";
import { setShipmentStatus } from "../../store/actions/shipmentsActions";
import { api } from "../../utils/api";
import Selecter from "../../Components/Selecter/Selecter";
import { postCountry } from "../../store/actions/countriesActions";
import AuthGuard, { UserType } from "../../Guards/AuthGuard";

const shipmentStatuses = [
  "CREATED",
  "RECIEVED",
  "INTRANSIT",
  "COMPLETED",
  "CANCELLED",
];

function AdminPage() {
  // Gets state from shipment reducer.
  const { underWay, completed, cancelled, idOfShipmentUpdating } = useSelector((state: RootState) => state.shipmentsData);

  // Gets state from contry reducer.
  const countries = useSelector((state: RootState) => state.countriesData);

  const [newCountry, setNewCountry] = useState("");
  const [currentCountryId, setCurrentCountryId] = useState(0);
  const [currentCountry, setCurrentCountry] = useState("Select Country");
  const [newMultiplier, setMultiplier] = useState(0);
  const [updateMultiplier, setUpdateMultiplier] = useState(0);
  const [currentShipmentStatus, setCurrentShipmentStatus] = useState(-1);
  const [openShipment, setOpenShipment] = useState(-1);

  /**
   * Fetches shipments
   * Fetches completed shipments
   * Fetches cancelled shipments
   * Fetches countries
   */
  useEffect(() => {
    store.dispatch(fetchShipments());
    store.dispatch(fetchCompletedShipments());
    store.dispatch(fetchCancelledShipments());
    store.dispatch(fetchCountries());
  }, []);

  /**
   * Opens modal with information about clicked shipment.
   * @param id Id for the shipment thats clicked
   */
  function handleOpenModal(id: number) {
    setOpenShipment(id);
  }

  /**
   * Closes modal and sets openshipment state to -1.
   */
  function handleCloseModal() {
    setOpenShipment(-1);
  }

  /**
   * Sets currentCountryId state and country state to current input.
   * @param e Event
   */
  function handleCountryChange(e: any/* React.ChangeEvent<HTMLInputElement> */) {
    e.preventDefault();
    setCurrentCountryId(e.target.value);
    setCurrentCountry(e.target.selectedOptions[0].text);
  }

  /**
   * Sets updatedMultiplier state to current input value.
   */
  function handleMultiplierUpdateChange(e: React.ChangeEvent<HTMLInputElement>) {
    e.preventDefault();
    setUpdateMultiplier(parseInt(e.target.value));
  }

  /**
   * Sets newMulitplier state to current input value.
   * @param e Event
   */
  function handleMultiplierInputChange(e: React.ChangeEvent<HTMLInputElement>) {
    e.preventDefault();
    setMultiplier(parseInt(e.target.value));
  }

  /**
   * Handles onchange event when option changes in dropdown and updates newCountry state with the chosen option.
   * @param e event
   */
  function handleAddCountry(e: React.ChangeEvent<HTMLInputElement>) {
    e.preventDefault();
    setNewCountry(e.target.value);
  }
  
  /**
   * OnClick. Checks if new country name isn't an empty string & country multiplier is greater than 0.
   * If condition is true. Post new country to DB and resets newCountry state and newMultiplier state.
   * Else shows alertbox.  
   */
  function onBtnAddCountry() {
    if (newCountry !== "" && newMultiplier > 0) {
      store.dispatch(
        postCountry({ name: newCountry, countryMultiplier: newMultiplier })
      );
      setNewCountry("");
      setMultiplier(0);
    } else {
      alert("No country selected or multiplier zero or less!");
    }
  }

  /**
   * Checks if country id is valid and multiplier isn't 0. 
   * If condition true, updates country multiplier in DB
   * Else shows alertbox.
   */
  function onBtnUpdateMultiplier() {
    if (currentCountryId > 0 && updateMultiplier > 0) {
      store.dispatch(
        putCountry(
          { name: currentCountry, countryMultiplier: updateMultiplier },
          currentCountryId
        )
      );
      setUpdateMultiplier(0);
      setCurrentCountryId(0);

    } else {
      alert("No country selected or multiplier zero or less!");
    }
  }

  /**
   * Handles onChange when option changes in dropdown
   * Sets current shipment status state to chosen option. 
   * @param value Value of chosen option
   */
  function handleShipmentStatusChange(value: any) {
    setCurrentShipmentStatus(parseInt(value));
  }

  /**
   * Checks if shipmentstatus is valid 
   * If condition true update shipment status.
   * Else shows alertbox.
   */
  function onBtnUpdateShipmentStatus() {
    if (openShipment > -1 && currentShipmentStatus > -1) {
      store.dispatch(
        setShipmentStatus(openShipment, { status: currentShipmentStatus })
      );
      setOpenShipment(-1);
    } else{
      alert("No Shipment selected!");
      
    }
  }



  return (
    <div className="page">
      <div className="onlyForMargin">
        <div className="admin-page-shipments">
          <h1>Utility</h1>
          <div className="utility-column">
            <div className="utility">
              <h3>Update multiplier</h3>
              <br />
              <select className="countries" onChange={handleCountryChange}>
                <option className="select-country">Select Country</option>
                {countries.countries.map((x: Country, i: number) => (
                  <option key={i} value={x.id}>
                    {x.name}
                  </option>
                ))}
              </select>
              <br />
              <br />
              <p className="p-admin">New value</p>
              <input
                type="text"
                value={updateMultiplier}
                onChange={handleMultiplierUpdateChange}
                className="input-admin"
              ></input>
              <br />
              <br />
              <button className="btn primary" onClick={onBtnUpdateMultiplier}>
                Update Multiplier
              </button>
            </div>

            <div className="utility">
              <h3>Add new country</h3>
              <br />
              <p className="p-admin">New Country</p>
              <input
                type="text"
                value={newCountry}
                onChange={handleAddCountry}
                className="input-admin"
                placeholder="Country Name"
              ></input>
              <br />
              <br />
              <p className="p-admin">Multiplier</p>
              <input
                type="text"
                value={newMultiplier}
                onChange={handleMultiplierInputChange}
                className="input-admin"
              ></input>
              <br />
              <br />
              <button className="btn primary" onClick={onBtnAddCountry}>
                Add Country
              </button>
            </div>
          </div>
        </div>
      </div>

      <div className="admin-page-shipments">
        <h1>Admin</h1>
        <div className="admin-page-lists">
          <h3>Underway</h3>
          <div className="admin-page-list">
            {underWay.error ? (
              <p>{underWay.error}</p>
            ) : underWay.loading ? (
              <Loader />
            ) : (
              underWay.shipments.map((box: ShipmentType, i: number) => (
                <Shipment
                  key={box.id}
                  {...box}
                  onClick={() => handleOpenModal(box.id)}
                />
              ))
            )}
          </div>

          <h3>Completed</h3>
          <div className="admin-page-list">
            {completed.error ? (
              <p>{completed.error}</p>
            ) : completed.loading ? (
              <Loader />
            ) : (
              completed.shipments.map((box: ShipmentType, i: number) => (
                <Shipment
                  key={box.id}
                  {...box}
                  onClick={() => handleOpenModal(box.id)}
                />
              ))
            )}
          </div>

          <h3>Cancelled</h3>
          <div className="admin-page-list">
            {cancelled.error ? (
              <p>{cancelled.error}</p>
            ) : cancelled.loading ? (
              <Loader />
            ) : (
              cancelled.shipments.map((box: ShipmentType, i: number) => (
                <Shipment
                  key={box.id}
                  {...box}
                  onClick={() => handleOpenModal(box.id)}
                />
              ))
            )}
          </div>
        </div>
      </div>

      <Modal
        title="Order Info"
        open={openShipment >= 0}
        loading={idOfShipmentUpdating !== null}
        onCancel={handleCloseModal}
      >
        <RecentOrderModal id={openShipment} admin={api.Admin} />
        <div className="status-select-row">
          <Selecter 
            options={shipmentStatuses.map((x: string) => ({
              value: shipmentStatuses.indexOf(x),
              text: x,
            }))}
            onChange={handleShipmentStatusChange}
          />
          <button className="btn primary" onClick={onBtnUpdateShipmentStatus}>
            Change status
          </button>
        </div>
      </Modal>
    </div>
  );
}

export default AuthGuard(UserType.Admin, AdminPage);
