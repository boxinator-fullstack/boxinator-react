import { useState } from "react";
import { useAuth0 } from "@auth0/auth0-react";
import "./StartPage.scss";
import Modal from "../../Components/Modal/Modal";
import NewOrderModal from "../../Modals/NewOrderModal/NewOrderModal";
import { ShipmentEditType } from "../../Models/ShipmentsTypes";
import { api } from "../../utils/api";
import AuthGuard, { UserType } from "../../Guards/AuthGuard";

function StartPage() {
  // Auth0 hook to redirect when user clicks on login.
  const { loginWithRedirect } = useAuth0();
  const [open, setOpen] = useState(false);
  const [loading, setLoading] = useState(false);
  const [message, setMessage] = useState("");

  /**
   * Opens modal when user clicks on new order.
   */
  function handleOpenModal() {
    setOpen(true);
  }

 /**
  * Closes modal.
  */ 
  function handleCloseModal() {
    setOpen(false);
    setMessage("")
  }

  /**
   * Post new shipment to DB and closes modal.
   * @param newShipment values from input for new shipment
   */
  function handleNewShipmentSubmit(newShipment : ShipmentEditType) {
    setLoading(true);
    api.postShipment(newShipment)
      .then(() => {
        setLoading(false);
        setMessage("Order created")
      }).catch((error : Error) => {
        setLoading(false);
        setMessage(error.message)
      })
  }

  return (
    <div className="container-startpage page">
      <div className="info">
        <h1 className="title-start">Boxinator</h1>
        <p className="paragraph-start">
          Order your mystery boxes with us.
          <br/><br/>DISCLAIMER: We don't take any responsibility for lost packages.
        </p>
      </div>
      <div className="btn-startpage">
        <button className="btn primary" onClick={() => handleOpenModal()}>Create New Order</button>
        <button className="btn primary" onClick={() => loginWithRedirect()}>Login/Register</button>
      </div>

      <Modal title="New Order" loading={loading} error={message} open={open} onCancel={handleCloseModal}>
        <NewOrderModal isAuthenticated={false} onSubmit={handleNewShipmentSubmit}/>
      </Modal>
    </div>
  );
}

export default AuthGuard(UserType.LoggedOut, StartPage);
