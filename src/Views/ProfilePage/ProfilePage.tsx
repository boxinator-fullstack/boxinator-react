import { useEffect, useState } from "react";
import "./Profile.scss";
import store from "../../store";
import { fetchAccount, putAccount } from "../../store/actions/accountActions";
import { RootState } from "../../store/reducers";
import { useSelector } from "react-redux";
import Loader from "../../Components/Loader/Loader";
import AuthGuard, { UserType } from "../../Guards/AuthGuard";
import Selecter from "../../Components/Selecter/Selecter";
import { Country } from "../../Models/CountryTypes";
import { fetchCountries } from "../../store/actions/countriesActions";

function ProfilePage() {
  // Gets account state from account reducer.
  const { account: user, loading } = useSelector((state: RootState) => state.accountData);

  // Gets country state from country reducer.
  const countriesData = useSelector((state: RootState) => state.countriesData);
  const [selectedCountry, setSelectedCountry] = useState();

  /**
   * Fetches account for current user.
   * Fetches countries.
   */
  useEffect(() => {
    store.dispatch(fetchAccount());
    store.dispatch(fetchCountries());
  }, []);

  /**
   * When update button is clicked. Gets all values from inputs and updates users properties in DB. 
   * @param e Event
   */
  function handleOnSubmit(e: any): void {
    e.preventDefault();

    const firstName = e.target.firstname.value;
    const lastName = e.target.lastname.value;
    const email = e.target.email.value;
    const birthDate = e.target.birthdate.value;
    const zipCode = e.target.zipcode.value;
    const contactnumber = e.target.contactnumber.value;
    const countryId: number = parseInt(e.target.countryId.value);

    // Validation for birthdate. Cannot be future date.
    const todaysDate = new Date();
    var birthdate1 = new Date(birthDate);

    if (birthdate1 > todaysDate) {
      alert("Enter a valid date."); 
    } else {
      const updatedUser1 = {
        id: user.id,
        firstName,
        lastName,
        email,
        birthDate,
        zipCode,
        contactnumber,
        countryId,
      };
      store.dispatch(putAccount(updatedUser1, user.id));
    }
  }

  /**
   * When country option is changed sets selected country state to new country.
   * @param countryId Id for chosen country.
   */
  function handleCountryChange(countryId: string) {
    const country = countriesData.countries.find(
      (c: Country) => c.id === parseInt(countryId)
    );
    setSelectedCountry(country);
  }

  /**
   * Checks if loading is true. Sets loader until everything is loaded.
   */
  if (loading) {
    return <Loader />;
  }

  return (
    <div className="page">
      <div className="container-profile">
        <h1>Profile</h1>
        <form className="profile-form" onSubmit={handleOnSubmit}>
          <label className="label-profile">First Name</label>
          <input
            type="text"
            className="profile-input"
            name="firstname"
            id="firstname"
            defaultValue={user.firstName}
            required
          />

          <label className="label-profile">Last Name</label>
          <input
            type="text"
            className="profile-input"
            name="lastname"
            id="lastname"
            defaultValue={user.lastName}
            required
          />

          <label className="label-profile">E-mail</label>
          <input
            disabled
            type="email"
            className="profile-input"
            name="email"
            id="email"
            defaultValue={user.email}
            required
          />

          <label className="label-profile">Date of Birth</label>
          <input
            type="date"
            className="profile-input"
            name="birthdate"
            id="birthDate"
            defaultValue={user.birthDate ? user.birthDate.split("T")[0] : ""}
            required
          />

          <label className="label-profile">Country</label>
          <Selecter
            onChange={handleCountryChange}
            name="countryId"
            options={countriesData.countries.map((country: Country) => ({
              value: country.id,
              text: country.name,
            }))}
            defaultValue={user.countryId}
            required
          />

          <label className="label-profile">Zip Code</label>
          <input
            type="text"
            className="profile-input"
            name="zipcode"
            id="zipCode"
            defaultValue={user.zipCode}
            required
          />

          <label className="label-profile">Phone</label>
          <input
            type="text"
            className="profile-input"
            name="contactnumber"
            id="contactNumber"
            defaultValue={user.contactnumber}
            required
          />
          <button className="btn primary" type="submit">
            Update
          </button>
        </form>
      </div>
    </div>
  );
}

export default AuthGuard(UserType.User, ProfilePage);
