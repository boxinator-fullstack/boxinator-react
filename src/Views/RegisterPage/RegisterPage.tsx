import { useAuth0 } from '@auth0/auth0-react';

export default function RegisterPage() {
    const { loginWithRedirect, logout } = useAuth0()
    logout({returnTo: `${window.location.origin}/start`});
    loginWithRedirect();
    return null;
}
