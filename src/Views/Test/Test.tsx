import React, { useEffect, useState } from 'react'
import { useAuth0 } from '@auth0/auth0-react';
import Modal from '../../Components/Modal/Modal';
import NewOrderModal from '../../Modals/NewOrderModal/NewOrderModal';
import { api } from '../../utils/api';
import { Country } from '../../Models/CountryTypes';


export default function Test() {
    const [country, setCountry] = useState<Country[]>([])
    const [idToken, setIdToken] = useState("")
    const [token, setToken] = useState("")
    const [open, setOpen] = useState(false)
    const { loginWithPopup, logout, user, isAuthenticated, getAccessTokenSilently, getIdTokenClaims } = useAuth0()
    const [roles, setRole] = useState("");
  
    useEffect(() => {
      if (!isAuthenticated) {
        setToken("")
        return
      }
      getIdTokenClaims().then((t) => {setIdToken(t.__raw)})
      getAccessTokenSilently().then((t) => {setToken(t)})
      getIdTokenClaims().then((t) => {setRole(t["http://www.adminstuff.com/getRoles/roles"][0])})
    }, [isAuthenticated, getAccessTokenSilently, getIdTokenClaims])

    useEffect(() => {
      api.getCountry().then(c => setCountry(c));
    }, [])

    function handleClick() {
      api.putCountry({countryMultiplier: 7, name: "test2"}, 6)
    }


    return (
        <div>
            <button onClick={handleClick}>test</button>
            {country.map(c => <p key={c.id}>{c.id} {c.name} {c.countryMultiplier.toString()}</p>)}

            <button onClick={() => loginWithPopup()}>Login</button>
            <button onClick={() => logout()}>Logout</button>

            <p>{JSON.stringify(user)}</p>
            <h3>Auth: {(isAuthenticated) ? "True" : "False"}</h3>
            <h3>sub: {user?.sub} </h3>
            <h3>Email: {user?.email} verified={user?.email_verified ? "True" : "False"}</h3>
            <h3>{user?.website}</h3>
            <h3>Token</h3>
            <p style={{wordWrap:"break-word", width:"90%"}}>{token}</p>
            <h3>Id Token</h3>
            <p style={{wordWrap:"break-word", width:"90%"}}>{idToken}</p>
            <h3>Roles</h3>
            <p style={{wordWrap:"break-word", width:"90%"}}>{roles}</p>

            <button onClick={() => setOpen(true)}>open modal</button>
            <Modal open={open} onCancel={() => setOpen(false)}>
            </Modal>
        </div>
    )
}
