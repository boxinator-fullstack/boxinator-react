import { useState, useEffect } from 'react'
import Shipment from '../../Components/Shipment/Shipment'
import './MainPage.scss'
import Modal from '../../Components/Modal/Modal';
import RecentOrderModal from '../../Modals/RecentModal/RecentOrderModal';
import NewOrderModal from '../../Modals/NewOrderModal/NewOrderModal';
import { useSelector } from 'react-redux';
import { RootState } from '../../store/reducers';
import store from '../../store';
import { fetchCompletedShipments, fetchShipments, postShipments, setShipmentsPostError, setShipmentStatus } from '../../store/actions/shipmentsActions';
import { ShipmentEditType, ShipmentType } from '../../Models/ShipmentsTypes';
import Loader from '../../Components/Loader/Loader';
import { api } from '../../utils/api';
import AuthGuard, { UserType } from '../../Guards/AuthGuard';

function MainPage() {
    // Gets state from shipment reducer.
    const { underWay, completed, posting, postError, idOfShipmentUpdating } = useSelector((state : RootState) => state.shipmentsData)
    const [openShipment, setOpenShipment] = useState(-1);
    const [openNew, setOpenNew] = useState(false);

    /**
     * Fetches shipments from api.
     * Fetches completed shipments from api.
     */
    useEffect(() => {
        store.dispatch(fetchShipments());
        store.dispatch(fetchCompletedShipments())
    }, [])

    /**
     * Opens modal when shipment is clicked.
     * @param id Id of the clicked shipment.
     */
    function handleOpenModal(id: number) {
        setOpenShipment(id)
    }

    /**
     * Closes modal and sets openShipment state to -1.
     */
    function handleCloseModal(){
        setOpenShipment(-1)
    }

    /**
     * Post new shipment to DB and closes modal.
     * @param newShipment values from inputs for new shipment
     */
    function handleNewShipmentSubmit(newShipment : ShipmentEditType) {
        store.dispatch(postShipments(newShipment));
        setOpenNew(false)
    }

    /**
     * Closes new order modal.
     */
    function handleNewOrderClose() {
        store.dispatch(setShipmentsPostError(""))
        setOpenNew(false)
    }

    /**
     * Handles if cancel shipment is clicked and sets new status to cancelled.
     * @param id Id for clicked shipment
     */
    function handleCancelOrder(id : number) {
        store.dispatch(setShipmentStatus(id, { status: 4 }))
        setOpenShipment(-1)
    }

    return (
        <div className="page">
            <div className="main-page-shipments">

                <h1>Shipments</h1>

                <button className="btn primary" onClick={() => setOpenNew(true)}>Create New Order</button>

                <div className="main-page-lists">
                    <h3>On The Way</h3>
                    <div className="main-page-list">
                        {(underWay.error) ? 
                            <p>{underWay.error}</p> 
                            :
                            (underWay.loading) ? 
                                <Loader/> 
                                :
                                underWay.shipments.map((box : ShipmentType, i : number) => <Shipment key={box.id} {...box} onClick={() => handleOpenModal(box.id)} />)
                        }   
                    </div>
                    <h3>Recently</h3>
                    <div className="main-page-list" >
                        {(completed.error) ? 
                            <p>{completed.error}</p> 
                            :
                            (completed.loading) ? 
                                <Loader/> 
                                :
                                completed.shipments.map((box : ShipmentType, i : number) => <Shipment key={box.id} {...box} onClick={() => handleOpenModal(box.id)} />)
                        }
                    </div>
                </div>
            </div>
            
            <Modal title="Order Info" open={(openShipment >= 0)} loading={(idOfShipmentUpdating !== null)} onCancel={handleCloseModal}>
                <RecentOrderModal id={openShipment} admin={api.Admin} onOrderCancel={handleCancelOrder}/>
            </Modal>
            <Modal title="New Order" open={openNew} loading={posting} error={postError} onCancel={handleNewOrderClose}>
                <NewOrderModal onSubmit={handleNewShipmentSubmit} isAuthenticated={true}/>
            </Modal>
        </div>
    )
}

export default AuthGuard(UserType.User, MainPage)
