import { useEffect, useState } from 'react';
import './App.scss';
import Header from './Components/Header/Header';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Test from './Views/Test/Test';
import StartPage from './Views/StartPage/StartPage';
import ProfilePage from './Views/ProfilePage/ProfilePage';
import AdminPage from './Views/AdminPage/AdminPage';
import MainPage from './Views/MainPage/MainPage';
import { useAuth0 } from '@auth0/auth0-react';
import { api } from './utils/api';
import Loader from './Components/Loader/Loader';
import store from './store';
import { fetchAccount } from './store/actions/accountActions';
import RegisterPage from './Views/RegisterPage/RegisterPage';

function App() {
  const [loading, setLoading] = useState(true)
  const { isAuthenticated, getAccessTokenSilently, getIdTokenClaims, isLoading } = useAuth0();
  

  /**
   * Checks if user is authenticated.
   */
  useEffect(() => {
    if (isLoading) return;
    if (isAuthenticated) {
      getAccessTokenSilently()
        .then(token => {
          api.token = token;
          setLoading(false);
          store.dispatch(fetchAccount())
        })
        .catch(() => {
          api.token = "";
          setLoading(false);
        })
        getIdTokenClaims().then((t) => {api.Admin = (t["http://www.adminstuff.com/getRoles/roles"][0])})
    } else {
      api.token = "";
      api.Admin = "";
      setLoading(false);
    }
  }, [isAuthenticated, getAccessTokenSilently, getIdTokenClaims, isLoading])

  return (
    <div className="App">
      {(loading) ?
      <div className="start-loader">
        <h1>Boxinator</h1>
        <Loader/>
      </div>
      :
      <Router>
        <Header/>
        <Switch>
          <Route exact path="/" component={MainPage}/>
          <Route exact path="/start" component={StartPage}/>
          <Route exact path="/profile" component={ProfilePage}/>
          <Route exact path="/admin" component={AdminPage}/>
          <Route exact path="/register" component={RegisterPage} /> 

          <Route exact path="/test" component={Test}/>
          <Route exact path="*">
            <div className="start-loader">
              <h1>404 not found</h1>
            </div>
          </Route>
        </Switch>
      </Router>}
    </div>
  );
}

export default App;
