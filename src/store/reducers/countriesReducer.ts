import { Country } from "../../Models/CountryTypes";
import {
  FETCH_COUNTRIES,
  POST_COUNTRY,
  PUT_COUNTRY,
  PUT_COUNTRY_ERROR,
  POST_COUNTRY_ERROR,
  SET_COUNTRIES,
  ADD_COUNTRY,
} from "../actions/countriesActions";

type State = {
  loading: boolean;
  countries: Country[];
  error: string;
};

// Country state.
const initialState: State = {
  loading: false,
  countries: [],
  error: "",
};

// Reducer for country handling.
export function countriesReducer(state = initialState, action: any) {
  switch (action.type) {
    case FETCH_COUNTRIES:
      return {
        ...state,
        loading: true,
      };
    case SET_COUNTRIES:
      return {
        ...state,
        countries: action.payload,
        loading: false,
      };
    case ADD_COUNTRY:
      return {
        ...state,
        countries: [action.payload, ...state.countries],
      };
    case POST_COUNTRY:
      return {
        ...state,
      };
    case PUT_COUNTRY:
      return {
        ...state,
        countries: [...state.countries],
        loading: false,
      };
    case PUT_COUNTRY_ERROR:
      return {
        ...state,
        error: action.payload,
      };
    case POST_COUNTRY_ERROR:
      return {
        ...state,
        error: action.payload,
      };
    default:
      return state;
  }
}
