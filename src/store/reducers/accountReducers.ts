import { FETCH_ACCOUNT, PUT_ACCOUNT, SET_ACCOUNT, SET_ACCOUNT_ERROR } from '../actions/accountActions'

// Account state
const initalState = {
        error: "",
        loading: false,
        account: {}
}

// Reducer for account handling.
export function accountReducer(state = initalState, action: any) {

    switch(action.type){
        case FETCH_ACCOUNT:
            return {
                ...state,
                loading: true,
            }
        case SET_ACCOUNT:
            return {
                ...state,
                loading: false,
                account: action.payload
            }
        case SET_ACCOUNT_ERROR: 
            return {
                ...state,
                error: action.payload
            }
        case PUT_ACCOUNT:
            return {
                ...state,
                loading: true
        }
        default: 
            return state;
        
    }
}
