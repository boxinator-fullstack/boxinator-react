import { ShipmentDetailedType } from "../../Models/ShipmentsTypes"
import { FETCH_SHIPMENTS_DETAILS, SET_SHIPMENTS_DETAILS, SET_SHIPMENTS_DETAILS_ERROR } from "../actions/shipmentDetailsActions"


type State = {
    error : string;
    loading : boolean;
    shipment? : ShipmentDetailedType;
}

// Shipment details state.
const initialState = {    
    error: "",
    loading: false,
    shipment: undefined
}

// Reducer for shipment details handling.
export function shipmentDetailsReducer(state : State = initialState, action : any) {

    switch(action.type) {
        case FETCH_SHIPMENTS_DETAILS:
            return {
                ...state,
                loading: true,
                error: ""
            }
        case SET_SHIPMENTS_DETAILS:
            return {
                ...state,
                shipment: action.payload,
                loading: false,
                error: ""
            }
        case SET_SHIPMENTS_DETAILS_ERROR:
            return {
                ...state,
                error: action.payload
            }
        default:
            return state;
    }
}