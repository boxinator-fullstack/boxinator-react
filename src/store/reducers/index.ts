import { combineReducers } from "redux";
import { shipmentsReducer } from './shipmentsReducer'
import { countriesReducer } from './countriesReducer'
import { accountReducer } from "./accountReducers";
import { shipmentDetailsReducer } from "./shipmentDetailsReducer";

const appReducer = combineReducers({
    shipmentsData: shipmentsReducer,
    countriesData: countriesReducer,
    accountData: accountReducer,
    shipmentDetailsData: shipmentDetailsReducer
})

export type RootState = ReturnType<typeof appReducer>
export default appReducer
