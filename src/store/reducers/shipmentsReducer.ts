import { ShipmentType } from "../../Models/ShipmentsTypes";
import {
    ADD_CANCELLED_SHIPMENTS,
  ADD_COMPLETED_SHIPMENTS,
  ADD_SHIPMENTS,
  FETCH_CANCELLED_SHIPMENTS,
  FETCH_COMPLETED_SHIPMENTS,
  FETCH_SHIPMENTS,
  POST_SHIPMENTS,
  REMOVE_CANCELLED_SHIPMENTS,
  REMOVE_COMPLETED_SHIPMENTS,
  REMOVE_SHIPMENTS,
  SET_CANCELLED_SHIPMENTS,
  SET_CANCELLED_SHIPMENTS_ERROR,
  SET_COMPLETED_SHIPMENTS,
  SET_COMPLETED_SHIPMENTS_ERROR,
  SET_SHIPMENTS,
  SET_SHIPMENTSTATUS,
  SET_SHIPMENTS_ERROR,
  SET_SHIPMENTS_POST_ERROR,
} from "../actions/shipmentsActions";

type ShipmentsCollectionType = {
  error: string;
  loading: boolean;
  shipments: ShipmentType[];
};

type State = {
  posting: boolean;
  postError: string;
  idOfShipmentUpdating: number | null;
  underWay: ShipmentsCollectionType;
  completed: ShipmentsCollectionType;
  cancelled: ShipmentsCollectionType;
};

// Shipment state....
const initialState : State = {
  posting: false,
  postError: "",
  idOfShipmentUpdating: null,
  underWay: {
    error: "",
    loading: false,
    shipments: [],
  },
  completed: {
    error: "",
    loading: false,
    shipments: [],
  },
  cancelled: {
    error: "",
    loading: false,
    shipments: [],
  },
};

// Reducer for shipment handling.
export function shipmentsReducer(state: State = initialState, action: any) : State {
  switch (action.type) {
    case FETCH_SHIPMENTS:
      return {
        ...state,
        underWay: {
          ...state.underWay,
          loading: true,
        },
      };
    case FETCH_COMPLETED_SHIPMENTS:
      return {
        ...state,
        completed: {
          ...state.underWay,
          loading: true,
        },
      };
    case FETCH_CANCELLED_SHIPMENTS:
      return {
        ...state,
        cancelled: {
          ...state.underWay,
          loading: true,
        },
      };
    case SET_SHIPMENTS:
      return {
        ...state,
        underWay: {
          ...state.underWay,
          shipments: action.payload,
          loading: false,
        },
      };
    case ADD_SHIPMENTS:
      return {
        ...state,
        posting: false,
        underWay: {
          ...state.underWay,
          shipments: [action.payload, ...state.underWay.shipments],
        },
      };
    case SET_COMPLETED_SHIPMENTS:
      return {
        ...state,
        completed: {
          ...state.completed,
          shipments: action.payload,
          loading: false,
        },
      };
    case SET_SHIPMENTS_ERROR:
      return {
        ...state,
        underWay: {
          ...state.underWay,
          error: action.payload,
        },
      };
    case SET_COMPLETED_SHIPMENTS_ERROR:
      return {
        ...state,
        completed: {
          ...state.completed,
          error: action.payload,
        },
      };
    case POST_SHIPMENTS:
      return {
        ...state,
        posting: true,
      };
    case SET_SHIPMENTS_POST_ERROR:
      return {
        ...state,
        posting: false,
        postError: action.payload,
      };
    case SET_CANCELLED_SHIPMENTS:
      return {
        ...state,
        cancelled: {
          ...state.cancelled,
          shipments: action.payload,
          loading: false,
        },
      };
    case SET_CANCELLED_SHIPMENTS_ERROR:
      return {
        ...state,
        cancelled: {
          ...state.cancelled,
          error: action.payload,
        },
      };
    case REMOVE_SHIPMENTS:
        return {
            ...state,
            idOfShipmentUpdating: null,
            underWay: {
                ...state.underWay,
                shipments: state.underWay.shipments.filter((s) => s.id !== action.payload)
            },
        }
    case REMOVE_CANCELLED_SHIPMENTS:
        return {
            ...state,
            idOfShipmentUpdating: null,
            cancelled: {
                ...state.cancelled,
                shipments: state.cancelled.shipments.filter((s) => s.id !== action.payload)
            },
        }
    case REMOVE_COMPLETED_SHIPMENTS:
        return {
            ...state,
            idOfShipmentUpdating: null,
            completed: {
                ...state.completed,
                shipments: state.completed.shipments.filter((s) => s.id !== action.payload)
            },
        }
    case ADD_CANCELLED_SHIPMENTS:
        return {
            ...state,
            cancelled: {
                ...state.cancelled,
                shipments: [action.payload, ...state.cancelled.shipments],
            },
        };
    case ADD_COMPLETED_SHIPMENTS:
        return {
            ...state,
            completed: {
                ...state.completed,
                shipments: [action.payload, ...state.completed.shipments],
            },
        };
    case SET_SHIPMENTSTATUS:
        return {
            ...state,
            idOfShipmentUpdating: action.payload.id    
        }
    default:
      return state;
  }
}
