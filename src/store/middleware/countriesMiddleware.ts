import { api } from "../../utils/api";
import { FETCH_COUNTRIES, POST_COUNTRY, postCountryError, PUT_COUNTRY,  putCountryError, setCountries, addCountry} from "../actions/countriesActions";

// Middleware for countries.
export function countriesMiddleware({ dispatch } : any) {
    return function(next : any) {
        return function(action : any) {
            next(action);

            /**
             * Calls get country from api and sets countrydata to state.
             * Catches if error and send out error message.
             */
            if (action.type === FETCH_COUNTRIES) {
                api.getCountry()
                .then(country => dispatch(setCountries(country)))
                .catch((error : Error) => dispatch(postCountryError(error.message)));
            }

            /**
             * Calls post country from api and sets values for new country.
             * Catches if error and send out error message.
             */
            if (action.type === POST_COUNTRY) {
                api.postCountry(action.payload).then(country => dispatch(addCountry(country))).catch((error : Error) => dispatch(postCountryError(error.message)));
            }

            /**
             * Calls put country from api and updates countrydata with new values.
             * Catches if error and send out error message.
             */
            if (action.type === PUT_COUNTRY) {
                api.putCountry(action.payload, action.payload1).catch((error : Error) => dispatch(putCountryError(error.message)));
            }
        }
    }
}