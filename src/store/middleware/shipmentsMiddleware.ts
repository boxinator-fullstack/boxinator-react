
import { api } from "../../utils/api";
import { FETCH_COMPLETED_SHIPMENTS, FETCH_SHIPMENTS, setShipmentStatusError, setCompletedShipments, 
    setShipments, setShipmentsError, setCompletedShipmentsError, POST_SHIPMENTS, addShipment, SET_SHIPMENTSTATUS,
    FETCH_CANCELLED_SHIPMENTS, 
    setCancelledShipments,
    setCancelledShipmentsError, setShipmentsPostError, removeShipment, removeCanceledShipment, removeCompletedShipment, addCompletedShipment, addCanceledShipment} from "../actions/shipmentsActions";
import { RootState } from "../reducers";

// Middleware for shipments
export function shipmentsMiddleware({ dispatch, getState } : any) {
    return function(next : any) {
        return function(action : any) {
            next(action);

            /**
             * Calls get shipments from api and sets shipmentdata to state.
             * Catches if error and send out error message.
             */
            if (action.type === FETCH_SHIPMENTS) {
                api.getShipments()
                    .then(shipments => dispatch(setShipments(shipments)))
                    .catch((error : Error) => dispatch(setShipmentsError(error.message)));
            }

            /**
             * Calls get completed shipments from api and sets completed shipmentdata to state.
             * Catches if error and send out error message.
             */
            if (action.type === FETCH_COMPLETED_SHIPMENTS) {
                api.getCompletedShipments()
                    .then(shipments => dispatch(setCompletedShipments(shipments)))
                    .catch((error : Error) => dispatch(setCompletedShipmentsError(error.message)));
            }

            /**
             * Calls get cancelled shipments from api and sets canclled shipmentdata to state.
             * Catches if error and send out error message.
             */
            if (action.type === FETCH_CANCELLED_SHIPMENTS) {
                api.getCancelledShipments()
                    .then(shipments => dispatch(setCancelledShipments(shipments)))
                    .catch((error : Error) => dispatch(setCancelledShipmentsError(error.message)));
            }

            /**
             * Calls post shipment from api and sets values for new shipment.
             * Catches if error and send out error message.
             */
            if (action.type === POST_SHIPMENTS) {
                api.postShipment(action.payload)
                    .then(shipment => dispatch(addShipment(shipment)))
                    .catch((error : Error) => dispatch(setShipmentsPostError(error.message)));
            }

            /**
             * Calls post shipment statuses from api and sets values for new shipment statuses.
             * Catches if error and send out error message.
             */
            if (action.type === SET_SHIPMENTSTATUS) {
                
                api.postShipmentStatuses(action.id, action.payload)
                    .then(() => {
                        const state : RootState = getState()
                        const { cancelled, completed, underWay }= state.shipmentsData

                        const shipment = cancelled.shipments.find((s) => s.id === action.id) 
                                        || completed.shipments.find((s) => s.id === action.id) 
                                        || underWay.shipments.find((s) => s.id === action.id) 
                        
                        if (shipment) {
                            dispatch(removeCanceledShipment(action.id))
                            dispatch(removeShipment(action.id))
                            dispatch(removeCompletedShipment(action.id))
                            if (action.payload.status === 4) {
                                dispatch(addCanceledShipment(shipment))
                            } else if (action.payload.status === 3) {
                                dispatch(addCompletedShipment(shipment))
                            } else {
                                dispatch(addShipment(shipment)) 
                            }
                        }
                    })
                    .catch((error : Error) => dispatch(setShipmentStatusError(error.message)));
            }
        }
    }
}