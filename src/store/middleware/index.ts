import { applyMiddleware } from "redux";
import { shipmentsMiddleware } from "./shipmentsMiddleware";
import { accountMiddleware } from "./accountMiddleware";
import { countriesMiddleware } from "./countriesMiddleware";
import { shipmentDetailsMiddleware } from "./shipmentDetailsMiddleware";

export default applyMiddleware(shipmentsMiddleware, accountMiddleware, countriesMiddleware, shipmentDetailsMiddleware)

