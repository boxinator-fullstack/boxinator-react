import {api} from '../../utils/api';
import { FETCH_ACCOUNT, setAccount, setAccountError, PUT_ACCOUNT} from '../actions/accountActions';

// Middleware for account.
export function accountMiddleware({dispatch}: any) {
    return function(next: any){
        return function(action: any){
            next(action);
            
            /**
             * Calls get account from api and sets userdata to state.
             * Catches if error and send out error message.
             */
            if(action.type === FETCH_ACCOUNT){
                api.getAccount()
                    .then(account => {
                        account.countryId = account.country?.id;
                        dispatch(setAccount(account))
                    })
                    .catch((error: Error) => dispatch(setAccountError(error.message)))
            }
            
            /**
             * Calls put account from api and updates userdata with new values.
             * Cathes if error and send out error message.
             */
            if (action.type === PUT_ACCOUNT) {
                api.putAccount(action.payload, action.payload2).then(() => dispatch(setAccount(action.payload))).catch((error : Error) => dispatch(setAccountError(error.message)));
            }
        }
    }
}