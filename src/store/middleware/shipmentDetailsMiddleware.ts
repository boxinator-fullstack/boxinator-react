import { api } from "../../utils/api";
import { FETCH_SHIPMENTS_DETAILS, setDetailedShipments, setDetailedShipmentsError } from "../actions/shipmentDetailsActions";


// Middleware for shipmentdetails.
export function shipmentDetailsMiddleware({ dispatch } : any) {
    return function(next : any) {
        return function(action : any) {
            next(action);

            /**
             * Calls get shipment by id from api and sets shipentdetails to state.
             * Catches if error and send out error message.
             */
            if (action.type === FETCH_SHIPMENTS_DETAILS) {
                api.getShipmentById(action.payload)
                    .then(shipment => dispatch(setDetailedShipments(shipment)))
                    .catch((error : Error) => dispatch(setDetailedShipmentsError(error.message)));
            }
        }
    }
}