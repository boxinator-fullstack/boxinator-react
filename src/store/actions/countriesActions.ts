import { Country, CountryEdit, CountryCreate  } from "../../Models/CountryTypes"

export const FETCH_COUNTRIES = '[countries] FETCH-COUNTRIES'
export const POST_COUNTRY = '[countries] POST-COUNTRY'
export const PUT_COUNTRY = '[countries] PUT-COUNTRY'
export const POST_COUNTRY_ERROR = '[countries] POST-COUNTRY-ERROR'
export const PUT_COUNTRY_ERROR = '[countries] PUT-COUNTRY-ERROR'
export const SET_COUNTRIES = '[countries] SET-COUNTRIES'

export const ADD_COUNTRY = '[countries} ADD-COUNTRY'

export function fetchCountries() {
    return {
        type: FETCH_COUNTRIES
    }
}

export function postCountry(payload : CountryCreate) {
    return {
        type: POST_COUNTRY,
        payload
    }
}

export function addCountry(payload : Country) {
    return {
        type: ADD_COUNTRY,
        payload
    }
}

export function setCountries(payload : Country[]) {
    return {
        type: SET_COUNTRIES,
        payload
    }
}

export function putCountry(payload : CountryEdit, payload1 : number) {
    return {
        type: PUT_COUNTRY,
        payload,
        payload1
    }
}

export function postCountryError(payload : string) {
    return {
        type: POST_COUNTRY_ERROR,
        payload
    }
}

export function putCountryError(payload : string) {
    return {
        type: PUT_COUNTRY_ERROR,
        payload
    }
}