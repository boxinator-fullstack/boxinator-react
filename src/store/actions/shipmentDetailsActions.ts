import { ShipmentDetailedType } from "../../Models/ShipmentsTypes"

export const SET_SHIPMENTS_DETAILS = '[shipments-details] SET'
export const FETCH_SHIPMENTS_DETAILS = '[shipments-details] FETCH'
export const SET_SHIPMENTS_DETAILS_ERROR = '[shipments-details] SET'


export function fetchDetailedShipments(payload : number) {
    return {
        type: FETCH_SHIPMENTS_DETAILS,
        payload
    }
}

export function setDetailedShipments(payload : ShipmentDetailedType) {
    return {
        type: SET_SHIPMENTS_DETAILS,
        payload
    }
}

export function setDetailedShipmentsError(payload : string) {
    return {
        type: SET_SHIPMENTS_DETAILS_ERROR,
        payload
    }
}