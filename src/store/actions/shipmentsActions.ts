import { ShipmentStatusType } from "../../Models/ShipmentStatusTypes"
import { ShipmentEditType, ShipmentType } from "../../Models/ShipmentsTypes"

export const SET_SHIPMENTS = '[shipments] SET'
export const FETCH_SHIPMENTS = '[shipments] FETCH'
export const SET_SHIPMENTS_ERROR = '[shipments] SET-ERROR'

export const POST_SHIPMENTS = '[shipments] POST'
export const ADD_SHIPMENTS = '[shipments] ADD'
export const SET_SHIPMENTS_POST_ERROR = '[shipments] SET-POST-ERROR'

export const SET_COMPLETED_SHIPMENTS = '[shipments] SET-COMPLETED'
export const FETCH_COMPLETED_SHIPMENTS = '[shipments] FETCH-COMPLETED'
export const SET_COMPLETED_SHIPMENTS_ERROR = '[shipments] SET-COMPLETED-ERROR'

export const SET_SHIPMENTSTATUS = '[shipments] SET-SHIPMENTSTATUS'
export const SET_SHIPMENTSTATUSERROR = '[shipments] SET-SHIPMENTSTATUSERROR'

export const FETCH_CANCELLED_SHIPMENTS = '[shipments] FETCH-CANCELLED-SHIPMENTS'
export const SET_CANCELLED_SHIPMENTS = '[shipments] SET-CANCELLED-SHIPMENTS'
export const SET_CANCELLED_SHIPMENTS_ERROR = '[shipments] SET-CANCELLED-SHIPMENTS-ERROR'


export const ADD_COMPLETED_SHIPMENTS = '[shipments] ADD-COMPLETED'
export const ADD_CANCELLED_SHIPMENTS = '[shipments] ADD-CANCELLED'

export const REMOVE_SHIPMENTS = '[shipments] REMOVE'
export const REMOVE_COMPLETED_SHIPMENTS = '[shipments] REMOVE-COMPLETED';
export const REMOVE_CANCELLED_SHIPMENTS = '[shipments] REMOVE-CANCELLED';


export function fetchShipments() {
    return {
        type: FETCH_SHIPMENTS
    }
}

export function postShipments(payload : ShipmentEditType) {
    return {
        type: POST_SHIPMENTS,
        payload
    }
}

export function addShipment(payload : ShipmentType) {
    return {
        type: ADD_SHIPMENTS,
        payload
    }
}

export function setShipments(payload : ShipmentType[]) {
    return {
        type: SET_SHIPMENTS,
        payload
    }
}

export function setShipmentsError(payload : string) {
    return {
        type: SET_SHIPMENTS_ERROR,
        payload
    }
}

export function fetchCompletedShipments() {
    return {
        type: FETCH_COMPLETED_SHIPMENTS
    }
}

export function fetchCancelledShipments() {
    return {
        type: FETCH_CANCELLED_SHIPMENTS
    }
}

export function setCompletedShipments(payload : ShipmentType[]) {
    return {
        type: SET_COMPLETED_SHIPMENTS,
        payload
    }
}

export function setCancelledShipments(payload : ShipmentType[]) {
    return {
        type: SET_CANCELLED_SHIPMENTS,
        payload
    }
}

export function setCompletedShipmentsError(payload : string) {
    return {
        type: SET_COMPLETED_SHIPMENTS_ERROR,
        payload
    }
}

export function  setShipmentsPostError(payload : string) {
    return {
        type: SET_SHIPMENTS_POST_ERROR,
        payload
    }
}
export function setCancelledShipmentsError(payload : string) {
    return {
        type: SET_CANCELLED_SHIPMENTS_ERROR,
        payload
    }
}

export function setShipmentStatus(id : number, payload : ShipmentStatusType) {
    return {
        type: SET_SHIPMENTSTATUS,
        id,
        payload
    }
}

export function setShipmentStatusError(payload: string) {
    return {
        type: SET_SHIPMENTSTATUSERROR,
        payload
    }
}

export function removeShipment(payload: number) {
    return {
        type: REMOVE_SHIPMENTS,
        payload
    }
}

export function removeCanceledShipment(payload: number) {
    return {
        type: REMOVE_CANCELLED_SHIPMENTS,
        payload
    }
}

export function removeCompletedShipment(payload: number) {
    return {
        type: REMOVE_COMPLETED_SHIPMENTS,
        payload
    }
}

export function addCanceledShipment(payload : ShipmentType) {
    return {
        type: ADD_CANCELLED_SHIPMENTS,
        payload
    }
}

export function addCompletedShipment(payload : ShipmentType) {
    return {
        type: ADD_COMPLETED_SHIPMENTS,
        payload
    }
}