import { Account, AccountEdit } from "../../Models/AccountTypes"

export const SET_ACCOUNT = '[account] SET'
export const PUT_ACCOUNT = '[account] PUT'
export const FETCH_ACCOUNT = '[account] FETCH'
export const SET_ACCOUNT_ERROR = '[account] SET-ERROR'


export function fetchAccount() {
    return {
        type: FETCH_ACCOUNT
    }
}

export function putAccount(payload: AccountEdit, payload2: number){
    return {
        type: PUT_ACCOUNT,
        payload2,
        payload
    }
}

export function setAccount(payload: Account){
    return {
        type: SET_ACCOUNT,
        payload
    }
}

export function setAccountError(payload: string){
    return {
        type: SET_ACCOUNT_ERROR,
        payload
    }
}